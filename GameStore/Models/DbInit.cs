﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using GameStore.Models.Entity;

namespace GameStore.Models
{
    public class DbInit: DropCreateDatabaseIfModelChanges<Context>
    {
        protected override void Seed(Context context)
        {
            context.Roles.Add(new Role { Id = 1, Name = "admin" });
            context.Roles.Add(new Role { Id = 2, Name = "user" });

            User user1 = new User{ Id =1, Username = "admin", Password = "admin", RoleId = 1};
            User user2 = new User { Id = 2, Username = "trol", Password = "trol", RoleId = 2 };
            User user3 = new User { Id = 3, Username = "123", Password = "123", RoleId = 2 };
            context.Users.Add(user1);
            context.Users.Add(user2);
            context.Users.Add(user3);

            Category cat1 = new Category { Name = "Симулятор" };
            Category cat2 = new Category { Name = "Шутер" }; 
            Category cat3 = new Category { Name = "RPG" };
            Category cat4 = new Category { Name = "Новое" };

            context.Categories.Add(cat1);
            context.Categories.Add(cat2);
            context.Categories.Add(cat3);


            List<Game> games = new List<Game>
            {
                new Game {
                    Name = "SimCity",
                    Description = "Градостроительный симулятор снова с вами! Создайте город своей мечты",
                    Categories = new List<Category> {cat1 },
                    Price = 1499
                },
                new Game {
                    Name = "TITANFALL",
                    Description = "Эта игра перенесет вас во вселенную, где малое противопоставляется большому, природа – индустрии, а человек – машине",
                    Categories = new List<Category> {cat2 },
                    Price =2299
                },
                new Game {
                    Name = "Battlefield 4",
                    Description = "Battlefield 4 – это определяющий для жанра, полный экшена боевик, известный своей разрушаемостью, равных которой нет",
                    Categories = new List<Category> {cat2 },
                    Price =899.4M },
                new Game {
                    Name = "The Sims 4",
                    Description = "В реальности каждому человеку дано прожить лишь одну жизнь. Но с помощью The Sims 4 это ограничение можно снять! Вам решать — где, как и с кем жить, чем заниматься, чем украшать и обустраивать свой дом",
                    Categories = new List<Category> {cat1 },
                    Price = 15 },
                new Game {
                    Name = "Dark Souls 2",
                    Description = "Продолжение знаменитого ролевого экшена вновь заставит игроков пройти через сложнейшие испытания. Dark Souls II предложит нового героя, новую историю и новый мир.Лишь одно неизменно – выжить в мрачной вселенной Dark Souls очень непросто.",
                    Categories = new List<Category> {cat3 },
                    Price =949 },
                new Game {
                    Name = "The Elder Scrolls V: Skyrim",
                    Description = "'После убийства короля Скайрима империя оказалась на грани катастрофы. Вокруг претендентов на престол сплотились новые союзы, и разгорелся конфликт.К тому же, как предсказывали древние свитки, в мир вернулись жестокие и беспощадные драконы.Теперь будущее Скайрима и всей империи зависит от драконорожденного — человека, в жилах которого течет кровь легендарных существ.",
                    Categories = new List<Category> {cat3 },
                    Price =1399 },
                new Game {
                    Name = "FIFA 14",
                    Description = "Достоверный, красивый, эмоциональный футбол! Проверенный временем геймплей FIFA стал ещё лучше благодаря инновациям, поощряющим творческую игру в центре поля и позволяющим задавать её темп.",
                    Categories = new List<Category> {cat1 },
                    Price =699 },
                new Game {
                    Name = "Need for Speed Rivals",
                    Description = "Забудьте про стандартные режимы игры. Сотрите грань между одиночным и многопользовательским режимом в постоянном соперничестве между гонщиками и полицией. Свободно войдите в мир, в котором ваши друзья уже участвуют в гонках и погонях.",
                    Categories = new List<Category> {cat1 },
                    Price =15 },
                new Game {
                    Name = "Crysis 3",
                    Description = "Действие игры разворачивается в 2047 году, а вам предстоит выступить в роли Пророка.",
                    Categories = new List<Category> {cat2 },
                    Price =1299},
            };
            Game game1 = new Game
            {
                Name = "Dead Space 3",
                Description = "В Dead Space 3 Айзек Кларк и суровый солдат Джон Карвер отправляются в космическое путешествие, чтобы узнать о происхождении некроморфов.",
                Categories = new List<Category> { cat2 },
                Price = 499
            };
            games.ForEach(d => context.Games.Add(d));
            context.Orders.Add(new Order { User = user2, Game = game1 });
            base.Seed(context);
        }
    }
}