﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace GameStore.Models.Person
{
    public class RegistrViewModel
    {
        [Required(ErrorMessage ="Поле должно быть заполненно")]
        public string Username { get; set; }

        [Required(ErrorMessage = "Поле должно быть заполненно")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Required(ErrorMessage = "Поле должно быть заполненно")]
        [Compare("Password", ErrorMessage = "Пароли не совпадают")]
        public string ConfPassword { get; set; }
    }
} 