﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Security;

namespace GameStore.Models.Person
{
    public class LoginViewModel
    {
        [Required(ErrorMessage = "Поле должно быть заполненно")]
        public string Username { get; set; }

        [Required(ErrorMessage = "Поле должно быть заполненно")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}