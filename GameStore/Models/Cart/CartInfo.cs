﻿using GameStore.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GameStore.Models.Cart
{
    public class CartInfo
    {
        public IEnumerable<Order> Orders { get; set; }
        public string ReturnUrl { get; set; }
        public List<OrderDetails> orderGames = null;

        public CartInfo(IEnumerable<Order> orders, string returnUrl)
        {
            Orders = orders;
            ReturnUrl = returnUrl;
            orderGames = Orders.GroupBy(m => m.Game).Select(m => new OrderDetails { Game = m.Key, Quant = m.Count() }).ToList();
        }

        public class OrderDetails
        {
            public Game Game { get; set; }
            public int Quant { get; set; }
            public decimal Cost { get { return Quant * Game.Price; } }
        }

        public Decimal TotalCost
        {
            get { return CalcTotalValue();}
        }

        public List<OrderDetails> OrderGames
        {
            get { return orderGames; }
        }

        private Decimal CalcTotalValue()
        {
            Decimal price = 0;
            orderGames.ForEach(m => price += m.Cost);
            return price;
        }
    }
}