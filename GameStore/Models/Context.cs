﻿using GameStore.Models.Entity;
using System.Data.Entity;

namespace GameStore.Models
{
    public class Context:DbContext
    {
        public Context() : base("GameDB") { }

        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Game> Games { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Category> Categories { get; set; }
    }
}