﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GameStore.Models.Entity;

namespace GameStore.Models.Pagina
{
    public class GamesList
    {
        public IEnumerable<Game> Games { get; set; }
        public PageInfo PageInfo { get; set; }
        public string Category { get; set; }
    }
}