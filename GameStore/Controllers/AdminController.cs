﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GameStore.Models;
using GameStore.Models.Entity;

namespace GameStore.Controllers
{
    [Authorize(Roles ="admin")]
    public class AdminController : Controller
    {
        private Context db = new Context();

        // GET: Admin
        public ActionResult Index()
        {
            return View(db.Games.Include(m => m.Categories).ToList());
        }

        private MultiSelectList GetCategories(int[] selectedValues)
        {
           // var categories = db.Categories.OrderBy(m => m.Name).Select(m => new { m.CategoryId, m.Name }).ToList();
            var categories = (from m in db.Categories select new { m.CategoryId, m.Name }).ToList();
            var list = new MultiSelectList(categories, "CategoryId", "Name", selectedValues);
            return list;
        }

        // GET: Admin/Create
        public ActionResult Create()
        {
            ViewBag.Categories = GetCategories(null);
            return View();
        }

        // POST: Admin/Create
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "GameId,Name,Description,Category,Price")] Game game, int[] categories)
        {
            if (ModelState.IsValid)
            {
                foreach(var catId in categories)
                {
                    game.Categories.Add(db.Categories.Find(catId));
                }
                db.Games.Add(game);
                db.SaveChanges();
                TempData["message"] = string.Format("Игра \"{0}\" была успешно занесена в базу", game.Name);
                return RedirectToAction("Index");
            }
            ViewBag.Categories = GetCategories(categories);
            return View(game);
        }

        // GET: Admin/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var game = db.Games.Include(m => m.Categories).FirstOrDefault(m => m.GameId == id);
            if (game == null)
            {
                return HttpNotFound();
            }
            var categoriesGame = game.Categories.Select(m => m.CategoryId).ToArray();
            ViewBag.Categories = GetCategories(categoriesGame);
            return View(game);
        }

        // POST: Admin/Edit/5
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "GameId,Name,Description,Category,Price")] Game game, int[] categories)
        {
            if (ModelState.IsValid)
            {
                var newGame = db.Games.Include(m => m.Categories).First(m=> m.GameId==game.GameId);
                newGame.Name = game.Name;
                newGame.Description = game.Description;
                newGame.Price = game.Price;

                newGame.Categories.Clear();
                if (categories != null)
                {
                    foreach (var cat in db.Categories.Where(m => categories.Contains(m.CategoryId)))
                    {
                        newGame.Categories.Add(cat);
                    }
                }
                db.Entry(newGame).State = EntityState.Modified;
                db.SaveChanges();
                TempData["message"] = string.Format("Изменения в игре \"{0}\" были сохранены", game.Name);
                return RedirectToAction("Index");
            }
            ViewBag.Categories = GetCategories(categories);
            return View(game);
        }

        // GET: Admin/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Game game = db.Games.Include(m => m.Categories).FirstOrDefault(m => m.GameId==id);
            if (game == null)
            {
                return HttpNotFound();
            }
            return View(game);
        }

        // POST: Admin/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Game game = db.Games.Find(id);
            db.Games.Remove(game);
            db.SaveChanges();
            TempData["message"] = string.Format("Игра \"{0}\" была успешно удалена", game.Name);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
