﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using GameStore.Models;
using GameStore.Models.Person;
using System.Data.Entity;
using GameStore.Models.Entity;
using GameStore.Models.Cart;

namespace GameStore.Controllers
{
    [Authorize(Roles = "user")]
    public class CartController : Controller
    {
        private Context db = new Context();

        public ViewResult Index(string returnUrl)
        {
            int id = User.Identity.GetUserId<int>();
            IEnumerable<Order> ordersUser = db.Orders.Where( m => m.UserId == id).Include(m => m.Game);
            var model = new CartInfo(ordersUser, returnUrl);
            return View(model);
        }
      
        public PartialViewResult Summary(string returnUrl)
        {
            int id = User.Identity.GetUserId<int>();
            IEnumerable<Order> ordersUser = db.Orders.Where(m => m.UserId == id).Include(m => m.Game);
            var model = new CartInfo(ordersUser, returnUrl);
            return PartialView(model);
        }

        [HttpPost]
        public RedirectToRouteResult AddToCart(int gameId, string returnUrl)
        {
            var game = db.Games.FirstOrDefault(m => m.GameId == gameId);
            var user = db.Users.Find(User.Identity.GetUserId<int>());

            var order = db.Orders.Create();
            order.Game = game;
            order.User = user;
            db.Orders.Add(order);
            db.SaveChanges();

            return RedirectToAction("Index", new { returnUrl });
        }

        [HttpPost]
        public RedirectToRouteResult RemoveFromCart(int gameId, string returnUrl)
        {

            var game = db.Games.FirstOrDefault(m => m.GameId == gameId);
            if (game != null)
            {
                int id = User.Identity.GetUserId<int>();
                var order = db.Orders.Where(m => m.GameId == game.GameId && m.UserId == id);
                if (order != null)
                {
                    db.Orders.RemoveRange(order);
                    db.SaveChanges();
                }
                db.SaveChanges();
            }
            return RedirectToAction("Index", new { returnUrl });
        }
    }
}