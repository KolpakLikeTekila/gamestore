﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GameStore.Models;
using GameStore.Models.Pagina;
using System.Data.Entity;
using GameStore.Models.Entity;

namespace GameStore.Controllers
{
    public class GameController : Controller
    {
        // GET: Game
        public ActionResult List( string category, int page =1)
        {
            int countItemPerPage = 3;
            int totalItem;
            IEnumerable<Game> games = null;
            using (Context db = new Context())
            {
                games = db.Games.Include(m => m.Categories);
                if (!String.IsNullOrEmpty(category) && !category.Equals("Все"))
                {
                    games = db.Categories.Where(m => m.Name.Equals(category)).Include(m => m.Games).SelectMany(m => m.Games);
                }
                totalItem = games.Count();
                games = games.Skip((page - 1) * countItemPerPage).Take(countItemPerPage).ToList();
            }
            PageInfo pageInfo = new PageInfo { PageNumber = page, PageSize = countItemPerPage, TotalItem = totalItem };
            GamesList model = new GamesList { Category = category, PageInfo = pageInfo, Games = games };

            return View(model);
        }

        
    }
}