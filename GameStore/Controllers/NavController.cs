﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GameStore.Models;
using GameStore.Models.Entity;
namespace GameStore.Controllers
{
    public class NavController : Controller
    {
        Context db = new Context();

        public PartialViewResult Menu(string category = null)
        {
            ViewBag.category = category;

            var cat = db.Categories.ToList();

            cat.Insert(0, new Category { Name = "Все" });

            return PartialView(cat);
        }
    }
}